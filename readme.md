#### DEEP LEARNING IS THE FUTURE!!!!

![](https://media.giphy.com/media/QxSzoprRmyJwtkdi6q/giphy.gif)


### About Me
- :detective: My name is Pavan and I study Computer Science at Amrita University.
- 🔭 I’m currently working on Path finding Visualizer GUI app.
- 🌱 I’m currently learning python, Deep Learning
- 💬 Ask me about Python, Deep Learning, Linux.
- 😄 Pronouns: He
- ⚡ Fun fact: I love watching anime and my favorite movie is [Kimi No Na wa](https://www.imdb.com/title/tt5311514/) meaning Your name.

---

### How i spend time

- I have immense interest in learning new things about deep learning.
- I watch anime as a recreational activity :eyes:
- I also Competitive programming, but i stopped doing it recently because i want to explore new things. Here is my [CodeForces profile](https://codeforces.com/profile/v-O_O-v) :eyes:

---

### TODO's

- :sunglasses: Make some cool projects.
- :smirk: participate in kaggle competitions
- :neutral_face: Learn things by searching in stack overflow.

---

### Things that are boon for mankind :muscle:

- StackOverflow
- Github
- Coursera
- CodeForces
---

